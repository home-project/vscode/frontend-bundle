## Frontend Bundle - Extension pack

Extensions which can be useful for angular developers.

### Installing package
_Note: This package is not published so it's not available on marketplace of VS Code_   

If you wish to use this extension pack run the following command in the foleder where the frontend-bundle-0.0.1.vsix file is placed.
```code --install-extension frontend-bundle-0.0.1.vsix```